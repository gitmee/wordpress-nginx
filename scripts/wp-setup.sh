#!/bin/bash
eval `cat ./wp.conf`
WEBSITE=$website
WEBSITE_HOME=$website_local_home
WEBSITE_TITLE=$website_title
WEBSITE_ADMIN=$website_admin
WEBSITE_ADMIN_PASS=$website_admin_pass
WEBSITE_ADMIN_EMAIL=$website_admin_email
NGINX_USER=$nginx_user
FPM_USER=$fpm_user
FPM_GROUP=$fpm_group
FPM_LISTEN_OWNER=$fpm_listen_owner
FPM_LISTEN_GROUP=$fpm_listen_group
DB_NAME=$db_name
DB_USER=$db_user
DB_HOST=$db_host
DB_PASS=$db_pass
CERT_PATH=$cert_path
KEY_PATH=$key_path
WP_CLI=$wp_cli_install_file
WP_CONTENT=$wp_content_file

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f "$WP_CLI" ];then
 echo -e "\033[33m Warning: You don't have a local wp-cli intall file, it will be downloaded from https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar."
fi
if [ ! -f "$WP_CONTENT" ];then
 echo -e "\033[33m Warning: You don't have a local WP content file(tar.gz). WP content will be downloaded by 'wp core download'."
fi

echo -e "\033[32m 1.Installing php-fpm \033[0m"
sudo apt-get install php7.2-fpm php7.2-common php7.2-mysql php7.2-xml php7.2-xmlrpc php7.2-curl php7.2-gd php7.2-imagick php7.2-cli php7.2-dev php7.2-imap php7.2-mbstring php7.2-opcache php7.2-redis php7.2-soap php7.2-zip -y
sudo sed -i "s/user = www-data/user = ${FPM_USER}/" /etc/php/7.2/fpm/pool.d/www.conf
sudo sed -i "s/group = www-data/group = ${FPM_GROUP}/" /etc/php/7.2/fpm/pool.d/www.conf
sudo sed -i "s/listen.owner = www-data/listen.owner = ${FPM_LISTEN_OWNER}/" /etc/php/7.2/fpm/pool.d/www.conf
sudo sed -i "s/listen.group = www-data/listen.group = ${FPM_LISTEN_GROUP}/" /etc/php/7.2/fpm/pool.d/www.conf
#sudo sed -i "s/www-data/${FPM_USER}/g" /etc/php/7.2/fpm/pool.d/www.conf
sudo service php7.2-fpm restart
sudo php-fpm7.2 -t

echo -e "\033[32m 2.Installing wp-cli \033[0m"
if [ ! -f "$WP_CLI" ];then
  cd ~
  curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
  chmod +x wp-cli.phar
  sudo mv wp-cli.phar /usr/local/bin/wp
else
  chmod +x $WP_CLI
  sudo mv $WP_CLI /usr/local/bin/wp
fi

if [ -x "$(command -v wp)" ]; then
  echo -e "\033[32m wp installed! \033[0m"
else
  echo -e "\033[31m Error: wp intall failed, please try again! \033[0m" >&2
  exit 1
fi

echo -e "\033[32m 3.Installing WordPress content \033[0m"
mkdir -p $WEBSITE_HOME/cache $WEBSITE_HOME/logs
if [ ! -f "$WP_CONTENT" ];then
  mkdir -p $WEBSITE_HOME/public
  cd $WEBSITE_HOME/public
  wp core download --locale=zh_CN
else
  tar -zxf $WP_CONTENT -C $WEBSITE_HOME
  mv $WEBSITE_HOME/wordpress $WEBSITE_HOME/public
  cd $WEBSITE_HOME/public
fi

wp core config --dbname=$DB_NAME --dbuser=$DB_USER --dbpass=$DB_PASS #dbhost
wp core install --url=$WEBSITE --title=$WEBSITE_TITLE --admin_user=$WEBSITE_ADMIN --admin_email=$WEBSITE_ADMIN_EMAIL --admin_password=$WEBSITE_ADMIN_PASS
sudo service php7.2-fpm restart

echo -e "\033[32m 4.Configuring nginx \033[0m"
sudo cp -r $SCRIPT_DIR/../nginx/global /etc/nginx/
sudo cp  $SCRIPT_DIR/../nginx/nginx.conf /etc/nginx/
sudo cp  $SCRIPT_DIR/../nginx/sites-enabled/example.com /etc/nginx/sites-enabled/$WEBSITE

sudo sed -i "s/www-data/$NGINX_USER/g" /etc/nginx/nginx.conf
sudo sed -i "s/example.com/$WEBSITE/g" /etc/nginx/sites-enabled/$WEBSITE
sudo sed -i "s|site_home|$WEBSITE_HOME|g" /etc/nginx/sites-enabled/$WEBSITE
sudo sed -i "s|your_cert_path|$CERT_PATH|" /etc/nginx/sites-enabled/$WEBSITE
sudo sed -i "s|your_key_path|$KEY_PATH|" /etc/nginx/sites-enabled/$WEBSITE

sudo nginx -t
sudo service nginx restart
echo -e "\033[32m Done! Try your website ${WEBSITE}! \033[0m"




