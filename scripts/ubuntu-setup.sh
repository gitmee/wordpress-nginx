#!/bin/bash
echo -e "\033[32m 1.Upgrading ubuntu \033[0m"
sudo apt update
sudo apt upgrade -y

echo -e "\033[32m 2.Setting up firewall using ufw \033[0m"
sudo apt install ufw -y
if [ -x "$(command -v ufw)" ]; then
  echo -e "\033[32m ufw installed! \033[0m"
else
  echo -e "\033[31m Error: ufw intall failed! \033[0m" >&2
  exit 1
fi
sudo ufw allow http
sudo ufw allow https
sudo ufw allow ssh
sudo ufw enable
sudo apt install fail2ban -y # fail2ban is used to update firewall rules to reject the IP addresses for a specified amount of time
echo -e "\033[32m ufw status: \033[0m"
sudo ufw show added
sudo ufw status verbose

echo -e "\033[32m 3.Installing nginx \033[0m"
sudo apt install -y software-properties-common
sudo add-apt-repository ppa:nginx/development -y
sudo apt update
sudo apt install -y nginx
if [ -x "$(command -v nginx)" ]; then
  echo -e "\033[32m nginx installed! \033[0m"
else
  echo -e "\033[31m Error: nginx is not installed! \033[0m" >&2
  exit 1
fi

echo -e "\033[32m 4.Installing mariadb \033[0m"
sudo apt install -y mariadb-server
if [ -x "$(command -v mysql)" ]; then
  echo -e "\033[32m mariadb-server installed! \033[0m"
else
  echo -e "\033[31m Error: mariadb-server install failed! \033[0m" >&2
  exit 1
fi

sudo mysql_secure_installation #interactions for the configuration of db such as root password

echo -e "\033[32m 5.Initializing database, need your mysql root password \033[0m"
eval `cat ./wp.conf`
DB_NAME=$db_name
DB_USER=$db_user
DB_HOST=$db_host
DB_PASS=$db_pass
CERT_PATH=$cert_path
KEY_PATH=$key_path
cat > wp.sql <<EOF

CREATE DATABASE $DB_NAME CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;
CREATE USER '$DB_USER'@'$DB_HOST' IDENTIFIED BY '$DB_PASS';
GRANT ALL PRIVILEGES ON $DB_NAME.* TO '$DB_USER'@'$DB_HOST';
FLUSH PRIVILEGES;
EOF

sudo mysql -u root -p < wp.sql
echo -e "\033[32m Database and access created! \033[0m"

echo -e "\033[32m 6.Generating self-signed cert and key \033[0m"
eval `cat ./wp.conf`
CERT_PATH=$cert_path
KEY_PATH=$key_path
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout $KEY_PATH -out $CERT_PATH
echo -e "\033[32m Cert and key created! \033[0m"
echo -e "\033[32m Cert: ${CERT_PATH} \033[0m"
echo -e "\033[32m Key: ${KEY_PATH} \033[0m"
echo -e "\033[32m --------------------------------------------------------- \033[0m"
echo -e "\033[32m Done! Please run wp-setup.sh to have your wordpress website created. \033[0m"

