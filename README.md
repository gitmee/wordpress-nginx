# wordpress-nginx

#### 介绍
基于wordpress和nginx搭建网站，运行在ubuntu18.04

#### 步骤
- 配置wp.conf
- 运行ubuntu-setup.sh (更新OS，安装Maria, Nginx，生成cert/key，初始化DB)
- 运行wp-setup.sh

#### TODO
- 发送邮件

#### 参考
- https://github.com/A5hleyRich/wordpress-nginx


