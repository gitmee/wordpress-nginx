# wordpress-nginx

#### Description
Build your own web site with wordpress and nginx, in ubuntu18.04.

#### TODO
- set up CA cert
- set up email

#### installation
- configure up wp.conf
- run ubuntu-setup.sh (OS upgrade，install MariaDB and Nginx，generate self-signed cert/key，create DB access)
- run wp-setup.sh (install php-fpm, wordpress, and config up nginx)


#### Ref
- https://github.com/A5hleyRich/wordpress-nginx