fastcgi_cache_path site_home/cache levels=1:2 keys_zone=example.com:100m inactive=60m;

server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	server_name example.com;

	root site_home/public;

    ## cert/key ##
	ssl_certificate your_cert_path;
    ssl_certificate_key your_key_path;

	index index.php;

	access_log site_home/logs/access.log;
	error_log site_home/logs/error.log;

	## Deny attempts to .* and .php ##
	location ~ /\. {
    	deny all;
    }
    location ~* /(?:uploads|files)/.*\.php$ {
    	deny all;
    }

    # Hide Nginx version
    server_tokens off;

    # iframe forbidden
    add_header X-Frame-Options "SAMEORIGIN" always;

    # MIME sniffing prevention
    add_header X-Content-Type-Options "nosniff" always;

    # Enable cross-site scripting filter in supported browsers
    add_header X-Xss-Protection "1; mode=block" always;

    include global/static-files.conf;

    ## Fastcgi cache rules ##
    fastcgi_cache_key "$scheme$request_method$host$request_uri";
    fastcgi_cache_use_stale error timeout updating invalid_header http_500; #returns cached content if 500 error
    fastcgi_ignore_headers Cache-Control Expires Set-Cookie;
    add_header Fastcgi-Cache $upstream_cache_status; # reponse cache status
    #check if need to skip
    set $skip_cache 0;
    if ($request_method = POST) {
        set $skip_cache 1;
    }
    if ($query_string != "") {
    	set $skip_cache 1;
    }
    if ($request_uri ~* "/wp-admin/|/wp-json/|/xmlrpc.php|wp-.*.php|/feed/|index.php|sitemap(_index)?.xml") {
    	set $skip_cache 1;
    }
    if ($http_cookie ~* "comment_author|wordpress_[a-f0-9]+|wp-postpass|wordpress_no_cache|wordpress_logged_in") {
    	set $skip_cache 1;
    }

	## SSL rules ##
	include global/ssl.conf;

	location / {
		try_files $uri $uri/ /index.php?$args;
	}

    location ~ \.php$ {
        try_files $uri =404;
        include global/fastcgi-params.conf;
        # Use the php pool defined in the upstream variable.
        fastcgi_pass   $upstream;
        # Skip cache
        fastcgi_cache_bypass $skip_cache;
        fastcgi_no_cache $skip_cache;
        # Caching, see keys_zone in fastcgi_cache_path.
        fastcgi_cache example.com;
        # Define caching time.
        fastcgi_cache_valid 60m;
	}

    # Rewrite robots.txt
    rewrite ^/robots.txt$ /index.php last;

    # Uncomment if using the fastcgi_cache_purge module and Nginx Helper plugin (https://wordpress.org/plugins/nginx-helper/)
    # location ~ /purge(/.*) {
    #	fastcgi_cache_purge example.com "$scheme$request_method$host$1";
    # }
}

# Redirect http to https
server {
	listen 80;
	listen [::]:80;
	server_name example.com www.example.com;

	return 301 https://example.com$request_uri;
}

# Redirect www to non-www
#server {
#	listen 443;
#	listen [::]:443;
#	server_name www.example.com;
#	return 301 https://example.com$request_uri;
#}
